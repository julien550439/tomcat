FROM calms/front AS front
FROM calms/back AS back

FROM tomcat:9.0.80-jdk11-corretto
COPY --from=front /dist/* /usr/local/tomcat/webapps/
COPY --from=back /dist/library.war /usr/local/tomcat/webapps/
RUN ls /usr/local/tomcat/webapps/
RUN sed -i 's/port="8080"/port="8082"/' /usr/local/tomcat/conf/server.xml
